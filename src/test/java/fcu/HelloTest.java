package fcu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloTest {

    @Test
    public void testSayHello(){
        Hello hello = new Hello();
        String testString = hello.sayHello("FCU");
        assertEquals("Hello FCU!", testString);
    }

}
