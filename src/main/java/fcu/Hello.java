package fcu;

public class Hello {

    public static void main(String[] args){
        Hello hello = new Hello();
        System.out.println(hello.sayHello("FCU"));
    }

    public String sayHello(String string){
        return "Hello " + string + "!";
    }

}
